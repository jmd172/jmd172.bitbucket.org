.. slideflow documentation master file, created by
   sphinx-quickstart on Mon Jan 14 19:42:39 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Slideflow Documentation
=======================

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   index
   pipeline
   project
   validation
   extract_tiles
   training
   evaluation
   appendix
   modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
